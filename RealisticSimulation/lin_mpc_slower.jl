using JuMP
using Ipopt
using Plots


h = 0.04 #0.02 might work aswell 0.04 does not miss many deadlines
J = 0.000154
M = 0.0
ma = 0.0
mp = 5.44/1000
la = 43/1000
lp = 32.3*2/1000
g = 9.81

alfa = J + (M + 1/3*ma + mp)*la^2
beta = (M + 1/3*mp)*lp^2
gamma = (M + 1/2*mp)*la*lp
delta = (M + 1/2*mp)*g*lp

"""
A = 
               x1          x2          x3          x4
   x1           1        0.04   -0.009062  -0.0001194
   x2           0           1     -0.4665   -0.009062
   x3           0           0       1.184     0.04243
   x4           0           0       9.484       1.184
 
  B = 
           u1
   x1   5.118
   x2   256.3
   x3  -4.922
   x4  -253.4
   """

function run_mpc(init_phi, init_phidot, init_theta, init_thetadot)
    model = Model(Ipopt.Optimizer)
	
    num_time_steps = 10 #10 for 0.02 and 6 for 0.04
    Mc = 3
    max_speed = 60
	Ts = 0.01 #Never used here
	
    @variables model begin
        phi[1:num_time_steps]
       	-max_speed <= phidot[1:num_time_steps] <= max_speed
        theta[1:num_time_steps]
        -max_speed <= thetadot[1:num_time_steps] <= max_speed
        u[1:num_time_steps-1]
    end

    # Dynamics constraints
    @constraint(model, [i=2:num_time_steps],
                phi[i] == phi[i-1] + 0.04*phidot[i-1]  -0.009062*theta[i-1]  -0.0001194*thetadot[i-1] + 5.118*u[i-1])
    @constraint(model, [i=2:num_time_steps],
                phidot[i] == phidot[i-1] -0.4665*theta[i-1] -0.009062*thetadot[i-1] + 256.3*u[i-1])
     @constraint(model, [i=2:num_time_steps],
                theta[i] == 1.184*theta[i-1] + 0.04243*thetadot[i-1] + -4.922*u[i-1])
     @constraint(model, [i=2:num_time_steps],
                thetadot[i] == 9.484*theta[i-1] + 1.184*thetadot[i-1] - 253.4*u[i-1])
          #@constraint(model, [i=Mc+1:num_time_steps-1],
          #u[i] == u[Mc])  
	
    # Cost function: minimize final position and final velocity
    #1 0.8 2 1
    @objective(model, Min, 
        0.8*sum(phidot[num_time_steps]).^2 + 2*sum(theta[num_time_steps]).^2 + sum(thetadot[num_time_steps]).^2)

    # Initial conditions:
    @constraint(model, phi[1] == init_phi)
    @constraint(model, phidot[1] == init_phidot)
    @constraint(model, theta[1] == init_theta)
    @constraint(model, thetadot[1] == init_thetadot)

    optimize!(model)
    return getvalue.(u)
end




# The robot's starting position and velocity
realphi = 1.0 #+ 2*(0.5-rand())
realphidot = 0.0 #+ 2*(0.5-rand())
realtheta = 0.1 #+ (0.5-rand())
realthetadot = 0.05 #+ 2*(0.5-rand())

thetas = []
phis = []
phidots = []
thetadots = []
ts = []
us = []
ti = 0.0

u = run_mpc(realphi, realphidot, realtheta, realthetadot)

println("\n \n \n \n \n -----------------Real simulation \n \n \n \n \n")

for i in 1:100
	global realtheta, realphi, realphidot, realthetadot, ti


	#if realtheta > 2*pi
	#	realtheta -= 2*pi
	#end
	
	
		
	ti += h
	push!(ts,ti)
	push!(thetas,realtheta)
	push!(phis,realphi)
	push!(thetadots,realthetadot)
	push!(phidots,realphidot)
	
	t = time()
	#if(mod(i,10) == 0)
	u = run_mpc(realphi, realphidot, realtheta, realthetadot)  
	#end
	println(time() - t) 
	push!(us,u[1])
	
	phi = realphi
	phidot = realphidot
	theta = realtheta
	thetadot = realthetadot
	
	realphi = phidot*h + phi
    realphidot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*gamma*(sin(theta)^2-1)*sin(theta)*phidot^2-2*beta^2*cos(theta)*sin(theta)*phidot*thetadot+beta*gamma*sin(theta)*thetadot^2-gamma*delta*cos(theta)*sin(theta)+beta*u[1]))+phidot
    realtheta = thetadot*h + theta
    realthetadot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*(alfa+beta*sin(theta)^2)*cos(theta)*sin(theta)*phidot^2+2*beta*gamma*(1-sin(theta)^2)*sin(theta)*phidot*thetadot-gamma^2*cos(theta)*sin(theta)*thetadot^2+delta*(alfa+beta*sin(theta)^2)*sin(theta)-gamma*cos(theta)*u[1]))+thetadot
end 



