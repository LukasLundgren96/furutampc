using JuMP
using Ipopt
using Plots


const h = 0.02 #0.02 might work aswell 0.04 does not miss many deadlines
J = 0.000154
M = 0.0
ma = 0.0
mp = 5.44/1000
la = 43/1000
lp = 32.3*2/1000
g = 9.81

const alfa = J + (M + 1/3*ma + mp)*la^2
const beta = (M + 1/3*mp)*lp^2
const gamma = (M + 1/2*mp)*la*lp
const delta = (M + 1/2*mp)*g*lp


function run_mpc(init_phi, init_phidot, init_theta, init_thetadot)
    model = Model(Ipopt.Optimizer)
	
	Ts = 0.02
    num_time_steps = 10 #10 for 0.02 and 6 for 0.04
    Mc = 5
    max_speed = 60
	
    @variables model begin
        phi[1:num_time_steps]
       	-max_speed <= phidot[1:num_time_steps] <= max_speed
        theta[1:num_time_steps]
        -max_speed <= thetadot[1:num_time_steps] <= max_speed
        u[1:num_time_steps-1]
    end

    # Dynamics constraints
    @NLconstraint(model, [i=2:num_time_steps],
                phi[i] == phidot[i-1]*Ts + phi[i-1])
    @NLconstraint(model, [i=2:num_time_steps],
                phidot[i] == Ts*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*gamma*(sin(theta[i-1])^2-1)*sin(theta[i-1])*phidot[i-1]^2-2*beta^2*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]+beta*gamma*sin(theta[i-1])*thetadot[i-1]^2-gamma*delta*cos(theta[i-1])*sin(theta[i-1])+beta*u[i-1]))+phidot[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                theta[i] == thetadot[i-1]*Ts + theta[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                thetadot[i] == Ts*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*(alfa+beta*sin(theta[i-1])^2)*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]^2+2*beta*gamma*(1-sin(theta[i-1])^2)*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]-gamma^2*cos(theta[i-1])*sin(theta[i-1])*thetadot[i-1]^2+delta*(alfa+beta*sin(theta[i-1])^2)*sin(theta[i-1])-gamma*cos(theta[i-1])*u[i-1]))+thetadot[i-1])
          #@constraint(model, [i=Mc+1:num_time_steps-1],
          #u[i] == u[Mc])  
	
    # Cost function: minimize final position and final velocity
    @objective(model, Min, 
        40*sum(theta[1:num_time_steps].^2) + 0.1*sum(phidot[1:num_time_steps].^2) + 0.05*sum(thetadot[1:num_time_steps].^2))

    # Initial conditions:
    @NLconstraint(model, phi[1] == init_phi)
    @NLconstraint(model, phidot[1] == init_phidot)
    @NLconstraint(model, theta[1] == init_theta)
    @NLconstraint(model, thetadot[1] == init_thetadot)

    optimize!(model)
    return getvalue.(u)
end




# The robot's starting position and velocity
realphi = 0.0 #+ 2*(0.5-rand())
realphidot = 0.0 #+ 2*(0.5-rand())
realtheta = -3.14 #+ (0.5-rand())
realthetadot = 0.0 #+ 2*(0.5-rand())

thetas = []
phis = []
phidots = []
thetadots = []
ts = []
us = []
ti = 0.0
u = run_mpc(realphi, realphidot, realtheta, realthetadot) 

println("RUNNING REAL SIMULATION HERE---------------------------------------------------------------")

for i in 0:1
	global realtheta, realphi, realphidot, realthetadot, ti, u
	ti += h
	push!(ts,ti)
	push!(thetas,realtheta)
	push!(phis,realphi)
	push!(thetadots,realthetadot)
	push!(phidots,realphidot)
	
	t = time()
	#if(mod(i,10) == 0)
	u = run_mpc(realphi, realphidot, realtheta, realthetadot)  
	#end
	println(time() - t) 
	push!(us,u[1])
	
	phi = realphi
	phidot = realphidot
	theta = realtheta
	thetadot = realthetadot
	
	realphi = phidot*h + phi
    realphidot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*gamma*(sin(theta)^2-1)*sin(theta)*phidot^2-2*beta^2*cos(theta)*sin(theta)*phidot*thetadot+beta*gamma*sin(theta)*thetadot^2-gamma*delta*cos(theta)*sin(theta)+beta*u[1]))+phidot
    realtheta = thetadot*h + theta
    realthetadot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*(alfa+beta*sin(theta)^2)*cos(theta)*sin(theta)*phidot^2+2*beta*gamma*(1-sin(theta)^2)*sin(theta)*phidot*thetadot-gamma^2*cos(theta)*sin(theta)*thetadot^2+delta*(alfa+beta*sin(theta)^2)*sin(theta)-gamma*cos(theta)*u[1]))+thetadot
end 



