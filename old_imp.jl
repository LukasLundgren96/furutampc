using JuMP
using Ipopt
using Plots
 
#Model variables
h = 0.02 #0.02 might work aswell 0.04 does not miss many deadlines
J = 0.000154
M = 0.0
ma = 0.0
mp = 5.44/1000
la = 43/1000
lp = 32.3*2/1000
g = 9.81

alfa = J + (M + 1/3*ma + mp)*la^2
beta = (M + 1/3*mp)*lp^2
gamma = (M + 1/2*mp)*la*lp
delta = (M + 1/2*mp)*g*lp

function run_mpc(init_phi, init_phidot, init_theta, init_thetadot, last_u)
    model = Model(Ipopt.Optimizer)
    num_time_steps = 10 #10 for 0.02 and 6 for 0.04
    Mc = 3
    max_speed = 60

    @variables model begin
        phi[1:num_time_steps]
       	-max_speed <= phidot[1:num_time_steps] <= max_speed
        theta[1:num_time_steps]
        -max_speed <= thetadot[1:num_time_steps] <= max_speed
        u[1:num_time_steps-1]
    end

    # Dynamics constraints
    @NLconstraint(model, [i=2:num_time_steps],
                phi[i] == phidot[i-1]*h + phi[i-1])
    @NLconstraint(model, [i=2:num_time_steps],
                phidot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*gamma*(sin(theta[i-1])^2-1)*sin(theta[i-1])*phidot[i-1]^2-2*beta^2*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]+beta*gamma*sin(theta[i-1])*thetadot[i-1]^2-gamma*delta*cos(theta[i-1])*sin(theta[i-1])+beta*u[i-1]))+phidot[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                theta[i] == thetadot[i-1]*h + theta[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                thetadot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*(alfa+beta*sin(theta[i-1])^2)*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]^2+2*beta*gamma*(1-sin(theta[i-1])^2)*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]-gamma^2*cos(theta[i-1])*sin(theta[i-1])*thetadot[i-1]^2+delta*(alfa+beta*sin(theta[i-1])^2)*sin(theta[i-1])-gamma*cos(theta[i-1])*u[i-1]))+thetadot[i-1])
          #@constraint(model, [i=Mc+1:num_time_steps-1],
          #u[i] == u[Mc])  
    
    # Cost function: minimize final position and final velocity
    @objective(model, Min, 
        4*sum(theta[num_time_steps].^2) + 1*phidot[num_time_steps]^2)

    # Initial conditions:
    @NLconstraint(model, phi[1] == init_phi)
    @NLconstraint(model, phidot[1] == init_phidot)
    @NLconstraint(model, theta[1] == init_theta)
    @NLconstraint(model, thetadot[1] == init_thetadot)

    optimize!(model)
    if termination_status(model) == MOI.LOCALLY_SOLVED
        return getvalue.(u)
    elseif size(last_u,1) > 1
        println("Returning last u")
        return last_u[2]
    else
        println("Returning zero")
        return 0.0
    end
end

#Global variables needed for running
ts = []
xs = []
θ_old = 0
revs = 0
starting_time = time()
ϕ, dϕdt, θ, dθdt = get_x()
θ -= pi
u = run_mpc(ϕ, dϕdt, θ, dθdt, [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]) 

enable!()

for i in 1:20
    #Next wakeup
    wakeup = time()+h
    
    #Get values
    ϕ, dϕdt, θ, dθdt = get_x()
    set_torque!(u[1])
    
    #Handle differences between model and actual process
    if θ_old > 1.5*pi && θ < 0.5*pi
        revs += 1
    elseif θ_old < 0.5*pi && θ > 1.5*pi
        revs -= 1
    end
    θ_old = θ
    θ = θ + revs*2*pi - pi
    
    #Calculate output and set it
    t = time()
    u = run_mpc(ϕ, dϕdt, θ, dθdt, u) 
    println("Execution time: ",(time() - t)) 
    println("U is: ",u)
    set_torque!(u[1])
    
    #Save values for plotting
    elapsed_time = time() - starting_time
    push!(ts, elapsed_time)
    push!(xs, [ϕ, dϕdt, θ, dθdt,u[1]])
    sleep(0.001)
    
    #Sleep
    sleeps = wakeup-time()
    if sleeps > 0
        println("Sleeping")
        sleep(sleeps)
    end
end 

set_torque!(0.0)
disable!()

#plotting
phis = [ x[1] for x in xs ]
thetas = [ x[3] for x in xs ];
thetadots = [ x[4] for x in xs ];
us = [ x[5] for x in xs ];
plot(ts, [thetas,us], xlabel = "Time [s]", ylabel = "Pendulum angle [rad]", label = ["theta" "u"],marker=:dots)
