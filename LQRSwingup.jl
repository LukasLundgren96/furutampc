function swingupWss()
    xs = []
    ts = []
    Ks1 = 0.0092
    Ks2 = 0.000001
    w = 3.705
    
   """ 
    K1 = -0.1506;
    K2 = -0.0167;
    K3 = -0.0007;
    K4 = -0.0016;
    """
    
    #K4=-0.0022;   K3=-0.0027;   K1=-0.1500;   K2=-0.0109;
    #K4=-0.0044;   K3=-0.0031;   K1=-0.1595;   K2=-0.0115;
    #K4n=-0.0065;   K3n=-0.0035;   K1n=-0.1689;   K2n=-0.0121;
    #K4n=-0.0125;   K3n=-0.0100;   K1n=-0.3882;   K2n=-0.0292;
    #K4n=-0.0084;   K3n=-0.0090;   K1n=-0.3556;   K2n=-0.0263;
    K4n=-0.0088;   K3n=-0.0094;   K1n=-0.3770;   K2n=-0.0286;
    count = 0
    
    total_time = 5.0
    elapsed_time = 0.0
    sleep_time = 0.01
    
    enable!()
    start_time = time()
    
    while elapsed_time < total_time
        ϕ, dϕdt, θ, dθdt = get_x()
        
        if abs(pi-abs(θ)) < 0.2
            ϕ = ϕ-2*pi*round(ϕ/(2*pi))
            θ = θ - pi 
            if count < 40
                u = -(K1n*θ+K2n*dθdt+K3n*ϕ+K4n*dϕdt)
                count += 1
            else
              u = -(K1n*θ+K2n*dθdt+K3n*ϕ+K4n*dϕdt)
            end
        else
            u = sign((cos(θ) + dθdt^2 / 2*w^2 - 1)*dθdt*cos(θ))*Ks1-Ks2*dϕdt;
            
        end
        push!(ts, elapsed_time)
        push!(xs, [ϕ, dϕdt, θ, dθdt,u]) 
        
        set_torque!(u)
        sleep(sleep_time)
        elapsed_time = time() - start_time        
    end
    
    set_torque!(0.0)
    disable!()
    
    phis = [ x[1] for x in xs ]
    thetas = [ x[3] for x in xs ];
    us = [ x[5] for x in xs ];
    plot(ts, [thetas,phis], xlabel = "Time [s]", ylabel = "Pendulum angle [rad]", label = "theta")
end

using Plots
swingupWss()
