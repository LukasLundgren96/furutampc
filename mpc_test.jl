using JuMP
using Ipopt


# run_mpc() takes the robot's current position and velocity
# and returns an optimized trajectory of position, velocity, 
# and acceleration. 
function run_mpc(initial_position, initial_velocity)
    model = Model(Ipopt.Optimizer)

    Δt = 0.01
    num_time_steps = 10
    max_acceleration = 0.5

    @variables model begin
        position[1:num_time_steps]
        velocity[1:num_time_steps]
        -max_acceleration <= acceleration[1:num_time_steps] <= max_acceleration
    end

    # Dynamics constraints
    @constraint(model, [i=2:num_time_steps],
                velocity[i] == velocity[i - 1] + acceleration[i - 1] * Δt)
    @constraint(model, [i=2:num_time_steps],
                position[i] == position[i - 1] + velocity[i - 1] * Δt)

    # Cost function: minimize final position and final velocity
    @objective(model, Min, 
        100 * sum(position[:, end].^2) + sum(velocity[:, end].^2))

    # Initial conditions:
    @constraint(model, position[1] .== initial_position)
    @constraint(model, velocity[1] .== initial_velocity)

    optimize!(model)
    return getvalue.(position), getvalue.(velocity), getvalue.(acceleration)
end




# The robot's starting position and velocity
q = 1.0
v = 0.0
Δt = 0.01

for i in 1:80
    # Run the MPC control optimization
    q_plan, v_plan, u_plan = run_mpc(q, v)
    
    # Apply the planned acceleration and simulate one step in time
    u = u_plan[1]
    global v += u * Δt
    global q += v * Δt
end


