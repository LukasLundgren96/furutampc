using JuMP
using Ipopt

model = Model(Ipopt.Optimizer)

# Define our constant parameters
Δt = 0.1
num_time_steps = 10
max_acceleration = 1.5

# Define our decision variables
@variables model begin
    position[1:2, 1:num_time_steps]
    velocity[1:2, 1:num_time_steps]
    -max_acceleration <= acceleration[1:2, 1:num_time_steps] <= max_acceleration
end

# Add dynamics constraints
@constraint(model, [i=2:num_time_steps, j=1:2],
            velocity[j, i] == velocity[j, i - 1] + acceleration[j, i - 1] * Δt)
@constraint(model, [i=2:num_time_steps, j=1:2],
            position[j, i] == position[j, i - 1] + velocity[j, i - 1] * Δt)

# Cost function: minimize final position and final velocity
@objective(model, Min, 
    100 * sum(2*position[:, end].^2) + sum(velocity[:, end].^2))

# Initial conditions:
@constraint(model, position[:, 1] .== [1, 0])
@constraint(model, velocity[:, 1] .== [0, -1])

optimize!(model)

q = getvalue.(position)
v = getvalue.(velocity)
u = getvalue.(acceleration)


using Plots
# Use the GR backend for Plots.jl, because it's fast
gr()
anim = @animate for i = 1:num_time_steps
    plot(q[1, :], q[2, :], xlim=(-1.1, 1.1), ylim=(-1.1, 1.1))
    plot!([q[1, i]], [q[2, i]], marker=(:hex, 6))
end

# The gif() function saves our animated plot to an animated gif
# Note: this may require you to have the `ffmpeg` program installed.
# On Ubuntu 16.04, you can get this with `sudo apt-get install ffmpeg`.
gif(anim, "img/mpc1.gif", fps = 30)
