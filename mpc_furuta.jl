using JuMP
using Ipopt
using Plots


h = 0.01
J = 0.000154
M = 0.0
ma = 0.0
mp = 5.44/1000
la = 43/1000
lp = 32.3*2/1000
g = 9.81

alfa = J + (M + 1/3*ma + mp)*la^2
beta = (M + 1/3*mp)*lp^2
gamma = (M + 1/2*mp)*la*lp
delta = (M + 1/2*mp)*g*lp

# run_mpc() takes the robot's current position and velocity
# and returns an optimized trajectory of position, velocity, 
# and acceleration. 
function run_mpc(init_phi, init_phidot, init_theta, init_thetadot)
    model = Model(Ipopt.Optimizer)
	
    Δt = 0.01
    num_time_steps = 20
    Mc = 3
	
    @variables model begin
        phi[1:num_time_steps]
       	-40 <= phidot[1:num_time_steps] <= 40
        theta[1:num_time_steps]
        -40 <= thetadot[1:num_time_steps] <= 40
        u[1:num_time_steps]
    end

    # Dynamics constraints
    @NLconstraint(model, [i=2:num_time_steps],
                phi[i] == phidot[i-1]*h + phi[i-1])
    @NLconstraint(model, [i=2:num_time_steps],
                phidot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*gamma*(sin(theta[i-1])^2-1)*sin(theta[i-1])*phidot[i-1]^2-2*beta^2*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]+beta*gamma*sin(theta[i-1])*thetadot[i-1]^2-gamma*delta*cos(theta[i-1])*sin(theta[i-1])+beta*u[i-1]))+phidot[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                theta[i] == thetadot[i-1]*h + theta[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                thetadot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*(alfa+beta*sin(theta[i-1])^2)*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]^2+2*beta*gamma*(1-sin(theta[i-1])^2)*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]-gamma^2*cos(theta[i-1])*sin(theta[i-1])*thetadot[i-1]^2+delta*(alfa+beta*sin(theta[i-1])^2)*sin(theta[i-1])-gamma*cos(theta[i-1])*u[i-1]))+thetadot[i-1])
          @constraint(model, [i=Mc+1:num_time_steps],
          u[i] == u[Mc]) 
	
	
	
	
    # Cost function: minimize final position and final velocity
    @objective(model, Min, 
        phi[num_time_steps]^2 + 0.2*phidot[num_time_steps]^2 + 4*theta[num_time_steps]^2 + 0.2*thetadot[num_time_steps]^2)

    # Initial conditions:
    @constraint(model, phi[1] .== init_phi)
    @constraint(model, phidot[1] .== init_phidot)
    @constraint(model, theta[1] .== init_theta)
    @constraint(model, thetadot[1] .== init_thetadot)

    optimize!(model)
    return getvalue.(u), getvalue.(phi), getvalue.(theta)
end




# The robot's starting position and velocity
realphi = 0.0
realphidot = 0.0
realtheta = -3.14
realthetadot = 0.0

# Run the MPC control optimization
u,phi,theta = run_mpc(realphi, realphidot, realtheta, realthetadot)    
plot(theta)


