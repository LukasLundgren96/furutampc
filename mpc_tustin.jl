using JuMP
using Ipopt
using Plots


h = 0.02 #0.02 might work aswell 0.04 does not miss many deadlines
J = 0.000154
M = 0.0
ma = 0.0
mp = 5.44/1000
la = 43/1000
lp = 32.3*2/1000
g = 9.81

alfa = J + (M + 1/3*ma + mp)*la^2
beta = (M + 1/3*mp)*lp^2
gamma = (M + 1/2*mp)*la*lp
delta = (M + 1/2*mp)*g*lp

# run_mpc() takes the robot's current position and velocity
# and returns an optimized trajectory of position, velocity, 
# and acceleration. 
function run_mpc(init_phi, init_phidot, init_theta, init_thetadot)
    model = Model(Ipopt.Optimizer)
	
    num_time_steps = 10 #10 for 0.02 and 6 for 0.04
    Mc = 3
    max_speed = 40
    max_torque = 0.1
	
    @variables model begin
        phi[1:num_time_steps]
       	-max_speed <= phidot[1:num_time_steps] <= max_speed
        theta[1:num_time_steps]
        -max_speed <= thetadot[1:num_time_steps] <= max_speed
        -max_torque <= u[1:num_time_steps-1] <= max_torque
    end

    # Dynamics constraints
    @NLconstraint(model, [i=2:num_time_steps],
                phi[i] == phidot[i-1]*h + phi[i-1])
    @NLconstraint(model, [i=2:num_time_steps],
                phidot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*gamma*(sin(theta[i-1])^2-1)*sin(theta[i-1])*phidot[i-1]^2-2*beta^2*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]+beta*gamma*sin(theta[i-1])*thetadot[i-1]^2-gamma*delta*cos(theta[i-1])*sin(theta[i-1])+beta*u[i-1]))+phidot[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                theta[i] == thetadot[i-1]*h + theta[i-1])
     @NLconstraint(model, [i=2:num_time_steps],
                thetadot[i] == h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta[i-1])^2)*(beta*(alfa+beta*sin(theta[i-1])^2)*cos(theta[i-1])*sin(theta[i-1])*phidot[i-1]^2+2*beta*gamma*(1-sin(theta[i-1])^2)*sin(theta[i-1])*phidot[i-1]*thetadot[i-1]-gamma^2*cos(theta[i-1])*sin(theta[i-1])*thetadot[i-1]^2+delta*(alfa+beta*sin(theta[i-1])^2)*sin(theta[i-1])-gamma*cos(theta[i-1])*u[i-1]))+thetadot[i-1])
          #@constraint(model, [i=Mc+1:num_time_steps-1],
          #u[i] == u[Mc])  
	
    # Cost function: minimize final position and final velocity
    @objective(model, Min, 
        0.5*sum(phi[num_time_steps].^2) + 4*sum(theta[num_time_steps].^2))

    # Initial conditions:
    @NLconstraint(model, phi[1] == init_phi)
    @NLconstraint(model, phidot[1] == init_phidot)
    @NLconstraint(model, theta[1] == init_theta)
    @NLconstraint(model, thetadot[1] == init_thetadot)

    optimize!(model)
    return getvalue.(u)
end




# The robot's starting position and velocity


realphi = 0.0 #+ 2*(0.5-rand())
realphidot = 0.0 #+ 2*(0.5-rand())
realtheta = 3.14 #+ (0.5-rand())
realthetadot = 0.0 #+ 2*(0.5-rand())

thetas = []
phis = []
phidots = []
thetadots = []
ts = []
us = []
ti = 0.0


for i in 1:150
	global realtheta, realphi, realphidot, realthetadot, ti
	
	controltheta = mod(realtheta,2*pi)
	#Mimic real systems weird theta handling
	
	
	ti += 0.02
	push!(ts,ti)
	push!(thetas,realtheta)
	push!(phis,realphi)
	push!(thetadots,realthetadot)
	push!(phidots,realphidot)
	
	t = time()
	u = run_mpc(realphi, realphidot, controltheta, realthetadot) 
	println(time() - t) 
	push!(us,u[1])
	
	phi = realphi
	phidot = realphidot
	theta = realtheta
	thetadot = realthetadot
	
	realphi = phidot*h + phi + (0.5-rand())/10
    realphidot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*gamma*(sin(theta)^2-1)*sin(theta)*phidot^2-2*beta^2*cos(theta)*sin(theta)*phidot*thetadot+beta*gamma*sin(theta)*thetadot^2-gamma*delta*cos(theta)*sin(theta)+beta*(u[1] - 0.00001*phidot)))+phidot + (0.5-rand())/10
    realtheta = thetadot*h + theta + (0.5-rand())/10
    realthetadot = h*(1/(alfa*beta-gamma^2+(beta^2+gamma^2)*sin(theta)^2)*(beta*(alfa+beta*sin(theta)^2)*cos(theta)*sin(theta)*phidot^2+2*beta*gamma*(1-sin(theta)^2)*sin(theta)*phidot*thetadot-gamma^2*cos(theta)*sin(theta)*thetadot^2+delta*(alfa+beta*sin(theta)^2)*sin(theta)-gamma*cos(theta)*(u[1] - 0.00001*phidot)))+thetadot + (0.5-1rand())/10
end 



